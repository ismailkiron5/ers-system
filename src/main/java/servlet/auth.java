package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Controller.employee;
import Controller.admin;

/**
 * Servlet implementation class auth
 */
public class auth extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	admin adm= new admin();
    public auth() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String output = request.getParameter("stringParameter");
		//String y[]=output.split(",");
		System.out.println(output);
		adm.connect();
		ArrayList<String> result=adm.ticket();
		//ArrayList<String> result=emp.ticket(usr);
		
		
		  response.setContentType("application/json");
		    response.setCharacterEncoding("UTF-8");
		    //System.out.println(hm);
		    response.getWriter().write(new Gson().toJson(result));

		//response.getWriter().append("Served at: ").append("Cristiano Ronaldo");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
