package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Controller.admin;

/**
 * Servlet implementation class status
 */
public class status extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	
	admin adm= new admin();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public status() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		adm.connect();
		String output = request.getParameter("stringParameter");
		System.out.println(output);
		//String []com=output.split("_");
		String user = request.getParameter("user");
		//System.out.println(user);
		adm.approve(Integer.parseInt(output));

		//response.getWriter().append("Served at: ").append(request.getContextPath());
		//response.sendRedirect("/Servle/html/admin.html");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		adm.connect();
		String output = request.getParameter("stringParameter");
		adm.deny(Integer.parseInt(output));
		//System.out.println(output+" delete");
	//	response.sendRedirect("/Servle/html/admin.html");

	}

}
