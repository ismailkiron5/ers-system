package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Controller.employee;

/**
 * Servlet implementation class insertion
 */
public class insertion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	employee em= new employee();
    public insertion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		em.connect();
		String output = request.getParameter("stringParameter");
		String []ent=output.split("_");
		//System.out.println(output);
		System.out.println("inserting Get");
		//em.insert(ent[1], ent[0]);
		
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		em.connect();
		//em.insert(ent[1], ent[0]);
        
		String des=request.getParameter("descrip");
		String bal=request.getParameter("amount");
		String cat=request.getParameter("cat");
		//String user = request.getParameter("stringParameter");
		String user = request.getCookies()[0].getValue();

		em.insert(user,des,cat,Integer.parseInt(bal));
		System.out.println(des+bal+cat+" "+user);
		
		response.sendRedirect("/Servle/html/home.html");
	}

}
