package model;

import java.util.ArrayList;

public interface AdminDao {

	ArrayList<String> ticket();

	void approve(int id);

	void deny(int id);

}
